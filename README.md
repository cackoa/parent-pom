# Zarządzanie wersjami wielu projektów

Przykład postępowania w przypadku posiadania wielu projektów mavenowych, które wzajemnie od siebie zależą.

Artykuł zawiera sposób postępowania w przypadku wydawania projektów, zarządzania wersjami poszczególnych zależności, utrzymania ich wersji.

## Założenia

Załóżmy, że posiadamy 3 projekty:

* parent-pom - to repozytorium
* project-A - [link do repozytorium project-A](https://bitbucket.org/cackoa/project-a)
* project-B - [link do repozytorium project-B](https://bitbucket.org/cackoa/project-b)

**parent-pom** - jest projektem klasycznego parenta.<br />
**project-A** - jest projektem, którego parentem jest **parent-pom**<br />
**project-B** - jest projektem, którego parentem jest **parent-pom** oraz zależy od **project-A**

Projekty te są osobnymi projektami (nie są mavenowymi modułami)
## Wersjonowanie kodu

### Warunki początkowe

Zakładamy, że faza developmentu postępuje w ten sposób, że zespół pracuje jakiś czas nad jakąś wersją. Załóżmy, że aktualnie jesteśmy w takiej sytuacji, że wszystkie projekty są w wersjach:

* parent-pom - [1.5.1-SNAPSHOT](https://bitbucket.org/cackoa/parent-pom/commits/0250fa20d203b24e1477f7f305f843cee8ea17ce)
* project-A - [2.4.2-SNAPSHOT](https://bitbucket.org/cackoa/project-a/commits/d9ba7e033329b7fdc7e1d888d54fa44773ac2d60)
* project-B - [3.8.4-SNAPSHOT](https://bitbucket.org/cackoa/project-b/commits/9b10f6a373635ad384f8db5ef6b20ea478f5964a)

Wszystkie projekty wskazują na swoje wersje SNAPSHOTOWE.

### Wydanie projektu

Po fazie developmentu gdzie nastąpiła seria commitów do każdego z projektów (zostały one zasymulowane w każdym projekcie) następuje wydanie wersji nad którą pracował zespół. Wydanie projektu oznacza dla nas:
* Podbicie wersji -SNAPSHOT do wersji stabilnej (np. 1.5.1-SNAPSHOT -> 1.5.1) dla każdego projektu
* Update zależności (parent-pom oraz dependency) na wersje wydane
* Podbicie wersji każdego wydanego projektu do kolejnej iteracji developerskiej (1.5.1 -> 1.5.2-SNAPSHOT)

Czynność tą można przeprowadzić ręcznie, choć przy dużej ilości projektów może być to kłopotliwe i czasochłonne.

Można to jednak zautomazyzować za pomocą dwóch pluginów mavena:
```xml
<plugin>
   <groupId>org.codehaus.mojo</groupId>
   <artifactId>versions-maven-plugin</artifactId>
   <version>2.5</version>
</plugin>
```
oraz 
```xml
<plugin>
   <groupId>org.apache.maven.plugins</groupId>
   <artifactId>maven-scm-plugin</artifactId>
   <version>1.9.5</version>
</plugin>
```
oraz
```xml
<plugin>
   <groupId>org.codehaus.mojo</groupId>
   <artifactId>versions-maven-plugin</artifactId>
   <version>2.5</version>
</plugin>
```

Wszystkie te pluginy zawarte są w **parent-pom** bądź są zawarte domyślnie w mavenie.

#### Wydanie stabilnej wersji

Musimy znać wzajemne zależności projektowe i budować je w odpowiedniej kolejności (tak jak to robi się domyślnie choćby budując projekty). Zaczynamy więc od **parent-pom** w którym ktoś dokonał [zmian](https://bitbucket.org/cackoa/parent-pom/commits/bdea5670bc53996369f723e0418e22efdfb4a6a6). Jako, że ten projekt nie zależy od innego możemy przejść do fazy właściwej - tj. podbicia wersji:

```
mvn clean && mvn release:clean && mvn -B release:prepare  && mvn release:perform
```
* **release:clean**<br />
Tak naprawdę jest to krok czyszczący śmieci wytwarzane przez kolejne dwa, w tym pliki release.properties i kopie zapasowe starych pomów.

* **release:prepare**<br />
Ten krok dokonuje odpowiednich sprawdzeń (np. czy zrobiliśmy commit) czy można wydać projekt, pyta nas o nowe wersje, podbija wersje tam gdzie trzeba, uruchamia testy, commituje zmiany do repo (nowe wersje projektu), tworzy taga w repo. WYMAGANE jest ustawienie gita po ssh!!! Parametr **-B** oznacza, że proces wydawania odbędzie się automatycznie bez zadawania jakiegokolwiek pytania. Jeśli zależy nam aby maven zapytał nas o numer kolejnej wersji, nazwę taga w GIT itp. należy usunąć ten parametr.

* **release:perform**<br />
buduje i wrzuca paczkę do nexusa. Ja nadpisałem tą funkcjonalność na **<goal>install</goal>** aby zbudować wydaną wersję wprost do folderu **target/checkout**

##### Wydanie parent-pom

Po wykonaniu powyższej instrukcji mavenowej zauważymy dwa commity:
* [[maven-release-plugin] prepare release parent-pom-1.5.1](https://bitbucket.org/cackoa/parent-pom/commits/53922e4bee31110472b0836395f376bcd422e3cb) otagowany automatycznie jako: **parent-pom-1.5.1**
* [[maven-release-plugin] prepare for next development iteration](https://bitbucket.org/cackoa/parent-pom/commits/1d8a31b3e99c6041cb7bf706c67fe7fc7db8f6ea)

Co się tu wydarzyło? Została wydana wersja 1.5.1, zainstalowana w lokalnym repo, a następnie zbudowana wersja 1.5.2-SNAPSHOT, która też została zbudowana i zainstalowana w repo.

##### Wydanie project-A

Aby wydać **projekt-A** należy zrobić jedną rzecz: zaktualizować wersję parenta do stabilnej (przed chwilą wydanej). Użyjemy do tego pluginu **versions**:

```
mvn clean versions:update-parent
```
Po tym rozkazie zostanie zaaktualizowany wpis parenta na **<version>1.5.1</version>**. Jako, że project-A nie zależy od innych projektów - możemy przejść do wydawania go:

```
mvn clean && mvn release:clean && mvn -B release:prepare  && mvn release:perform
```
Ponownie zostanie wydana wersja stabilna, zbudowana, zainstalowana w repo, otagowana i wcommitowana. Następnie wersja zostanie podbita do następnej developerskiej i zostanie zrobiony kolejny commit.

Przejście na wersje developerską niestety nie została przeprowadzona do końca. Należy zmienić wersję parenta na aktualną SNAPSHOT. Robimy to poleceniem:
```
mvn versions:update-parent versions:use-latest-versions -Dincludes=pl.release.example -DallowSnapshots=true clean install
```
Dwa ostatnie parametry ozaczają odpowiednio - włączenie updatu tylko dla naszych projektów oraz pozwolenie na znajdowanie projektów typu SNAPSHOT. Projekt oczywiście budujemy i instalujemy w repo.

Należy jednak jeszcze zmiany wrzucić do GIT:
```
mvn scm:add -Dincludes=pom.xml scm:checkin -Dmessage='Change to Snapshot after release'
```
##### Wydanie project-B

Tutaj mamy bardziej złożoną sytuację - projekt ten zależy od naszego **parent-pom** oraz od **project-A**. Taki scenariusz wymaga przed wydaniem wykonania:
* update parenta do wersji wydanej (1.5.1)
* update wszystkich dependency do wersji wydanych (project-A na wersję: 2.4.2)

Robi to komenda:
```
mvn versions:update-parent versions:use-latest-versions -Dincludes=pl.release.example
```

Potem wydajemy wersję tak jak ostatnio:

```
mvn clean && mvn release:clean && mvn -B release:prepare  && mvn release:perform
```

Podbijając wszystkie wersje na SNAPSHOT:
```
mvn versions:update-parent versions:use-latest-versions -Dincludes=pl.release.example -DallowSnapshots=true
```
Specjalnie pominąłem na końcu clean install gdyż straciłbym z folderu target/checkout/target wersję, którą pewnie chciałbym gdzieś opublikować (pamiętajmy, że oryginalnie release:perform publikuje do nexusa więc teoretycznie tego byśmy nie stracili)

Wrzucamy zmiany:
```
mvn scm:add -Dincludes=pom.xml scm:checkin -Dmessage='Change to Snapshot after release'
```

